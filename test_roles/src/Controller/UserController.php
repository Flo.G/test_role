<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{
    /**
     * @Route ("/users", name="users")
     */

    public function form(Request $request, EntityManagerInterface $entityManager , UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ROLE_USER']);
            $hashedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user-> setPassword($hashedPassword);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_login');

        }

        return $this->render('user/FormAccount.html.twig', [
            'UserForm' => $form->createView()
        ]);
    }

    /**
     * @Route ("/profile/{id}", name="profile_user")
     */
    public function index(User $user){
        return $this->render('user/pageUser.html.twig',[
            'profile_user'=>$user
        ]);
    }
}
