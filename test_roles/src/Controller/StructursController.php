<?php

namespace App\Controller;

use App\Entity\Structurs;
use App\Entity\User;
use App\Form\StructursType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class StructursController extends AbstractController
{
    /**
     * @Route("/structurs", name="structurs")
     */
    public function form(Request $request, EntityManagerInterface $entityManager , UserPasswordEncoderInterface $encoder)
    {
        $structurs = new Structurs();
        $form = $this->createForm(StructursType::class, $structurs);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $structurs->setRoles(['ROLE_ADMIN']);
            $hashedPassword = $encoder->encodePassword($structurs, $structurs->getPassword());
            $structurs-> setPassword($hashedPassword);
            $entityManager->persist($structurs);
            $entityManager->flush();

            return $this->redirectToRoute('app_login');

        }

        return $this->render('structurs/index.html.twig', [
            'StructursForm' => $form->createView()
        ]);
    }
}
