<?php

namespace App\Controller\Admin\Dashboard;

use App\Entity\Stockpiles;
use App\Entity\Structurs;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardNonAffilieController extends AbstractDashboardController
{
    /**
     * @Route("/naffilie", name="naffilie")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Test Roles');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        //zone admin--------------------------------------
        if($this->isGranted('ROLE_USER'));
        yield MenuItem::section('Administrateur');
        //structur menu
        yield MenuItem::subMenu('Structurs', 'fa fa-comment')->setSubItems([
            MenuItem::linkToCrud('Add Structurs', 'fa fa-file-text', Structurs::class)
                ->setAction('new'),
                //->setRole(['ROLE_ADMIN']),
            MenuItem::linkToCrud('Show Structurs detail', 'fa fa-comment', Structurs::class)
                ->setAction('detail')
            /*->setEntityId(4)*/,
            MenuItem::linkToCrud('Show Structurs ordered', 'fa fa-tags', Structurs::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
        ]);


    }
}
