<?php

namespace App\Controller\Admin\Dashboard;

use App\Entity\Category;
use App\Entity\Stockpiles;
use App\Entity\Structurs;
use App\Entity\User;
use App\Entity\Products;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DashboardSuperAdminController extends AbstractDashboardController
{
    /**
     * @Route("/super_admin", name="super_admin")
     */
    public function index(): Response
    {

        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(StructursCrudController::class)->generateUrl());

    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Easy51');
    }

    public function configureMenuItems(): iterable
    {
        if($this->isGranted('ROLE_SUPER_ADMIN'));
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');

        //zone super admin--------------------------------------
        yield MenuItem::section('Super Administrateur');

        //Users menu
        yield MenuItem::subMenu('Users', 'fa fa-comment')->setSubItems([
            MenuItem::linkToCrud('Add Users', 'fa fa-file-text', User::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Show Users detail', 'fa fa-comment', User::class)
                ->setAction('detail')
                /*->setEntityId(4)*/,
            MenuItem::linkToCrud('Show Users ordered', 'fa fa-tags', User::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
        ]);

        //category menu
        yield MenuItem::subMenu('Category', 'fa fa-comment')->setSubItems([
            MenuItem::linkToCrud('Add Category', 'fa fa-file-text', Category::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Show Category detail', 'fa fa-comment', Category::class)
                ->setAction('detail')
            /*->setEntityId(4)*/,
            MenuItem::linkToCrud('Show Category ordered', 'fa fa-tags', Category::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
        ]);

    }
}
