<?php

namespace App\Controller\Admin\Dashboard;

use App\Entity\Products;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardAffilieController extends AbstractDashboardController
{
    /**
     * @Route("/affilie", name="affilie")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Test Roles');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');

        //zone affilié--------------------------------------
        if($this->isGranted('ROLE_AFFILIATED_REGISS'));
        yield MenuItem::section('Afilliated User');
        //products menu
        yield MenuItem::subMenu('Products', 'fa fa-comment')->setSubItems([
            MenuItem::linkToCrud('Add Products', 'fa fa-file-text', Products::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Show Products detail', 'fa fa-comment', Products::class)
                ->setAction('detail')
            /*->setEntityId(4)*/,
            MenuItem::linkToCrud('Show Products ordered', 'fa fa-tags', Products::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
        ]);


        if ($this->getUser()) {
            yield MenuItem::linkToLogout('Logout', 'fa fa-exit');
        }
    }
}
