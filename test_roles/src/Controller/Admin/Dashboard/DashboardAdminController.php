<?php

namespace App\Controller\Admin\Dashboard;

use App\Entity\Stockpiles;
use App\Entity\Structurs;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardAdminController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Test Roles');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        //zone admin--------------------------------------
        if($this->isGranted('ROLE_ADMIN'));

        yield MenuItem::section('Administrateur');
        //structur menu
        yield MenuItem::subMenu('Structurs', 'fa fa-comment')->setSubItems([
            MenuItem::linkToCrud('Add Structurs', 'fa fa-file-text', Structurs::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Show Structurs detail', 'fa fa-comment', Structurs::class)
                ->setAction('detail')
            /*->setEntityId(4)*/,
            MenuItem::linkToCrud('Show Structurs ordered', 'fa fa-tags', Structurs::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
        ]);

        //stockpiles menu
        $structurs = New Structurs();

        yield MenuItem::subMenu('Stockpiles', 'fa fa-comment')->setSubItems([
            MenuItem::linkToCrud('Add Stockpiles', 'fa fa-file-text', Stockpiles::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Show Stockpiles detail', 'fa fa-comment', Stockpiles::class)
                ->setAction('detail')
            /*->setEntityId(4)*/,
            MenuItem::linkToCrud('Show Stockpiles ordered', 'fa fa-tags', Stockpiles::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
        ]);
    }
}
