<?php

namespace App\Controller\Admin;

use App\Entity\Structurs;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class StructursCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Structurs::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'Nom'),
            EmailField::new('email'),
            AssociationField::new('Admin_user_email'),//admin_user_id->admin_user_email || admin_user_fullname pour definir???
            TextEditorField::new('description'),
            MoneyField::new('big')->setCurrency('BIF')->hideOnForm(), //TODO setCurrency BIG
            TelephoneField::new('tel_number', 'Numéro de telephone'),
            AssociationField::new('Stockpiles', 'Lieux de stockages'),
        ];
    }

}
