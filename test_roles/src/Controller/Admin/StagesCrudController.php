<?php

namespace App\Controller\Admin;

use App\Entity\Stages;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StagesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Stages::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
