<?php

namespace App\Controller\Admin;

use App\Entity\Stockpiles;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StockpilesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Stockpiles::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
