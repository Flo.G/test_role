<?php

namespace App\Repository;

use App\Entity\Structurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Structurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Structurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Structurs[]    findAll()
 * @method Structurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StructurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Structurs::class);
    }

    // /**
    //  * @return Structurs[] Returns an array of Structurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Structurs
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
