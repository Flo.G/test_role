<?php

namespace App\Repository;

use App\Entity\Stockpiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stockpiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stockpiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stockpiles[]    findAll()
 * @method Stockpiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockpilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stockpiles::class);
    }

    // /**
    //  * @return Stockpiles[] Returns an array of Stockpiles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stockpiles
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
