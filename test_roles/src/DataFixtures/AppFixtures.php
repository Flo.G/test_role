<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 2; $i ++);
            $user = new User();
            $user ->setEmail($faker->email);
            $user ->setPassword ('1234');
            $user ->setRoles(['ROLE_USER','ROLE_SUPER_ADMIN']);
            $manager ->persist($user);

        $manager->flush();
    }
}
