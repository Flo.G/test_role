<?php

namespace App\Entity;

use App\Repository\StockpilesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockpilesRepository::class)
 */
class Stockpiles
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $postal_code;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $contact_user_id;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $contract_content = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deposit_value;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stock_commission;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stock_commission_owner_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maintenance_commission;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maintenance_commission_owner_id;

    /**
     * @ORM\OneToMany(targetEntity="Products", mappedBy="stockpile")
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity=Structurs::class)
     */
    private $structurs;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->structurs = new ArrayCollection();
        $this->structurs = new ArrayCollection();
        $this->structurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postal_code;
    }

    public function setPostalCode(int $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    public function getContactUserId(): ?int
    {
        return $this->contact_user_id;
    }

    public function setContactUserId(?int $contact_user_id): self
    {
        $this->contact_user_id = $contact_user_id;

        return $this;
    }

    public function getContractContent(): ?array
    {
        return $this->contract_content;
    }

    public function setContractContent(?array $contract_content): self
    {
        $this->contract_content = $contract_content;

        return $this;
    }

    public function getDepositValue(): ?int
    {
        return $this->deposit_value;
    }

    public function setDepositValue(?int $deposit_value): self
    {
        $this->deposit_value = $deposit_value;

        return $this;
    }

    public function getStockCommission(): ?int
    {
        return $this->stock_commission;
    }

    public function setStockCommission(?int $stock_commission): self
    {
        $this->stock_commission = $stock_commission;

        return $this;
    }

    public function getStockCommissionOwnerId(): ?int
    {
        return $this->stock_commission_owner_id;
    }

    public function setStockCommissionOwnerId(?int $stock_commission_owner_id): self
    {
        $this->stock_commission_owner_id = $stock_commission_owner_id;

        return $this;
    }

    public function getMaintenanceCommission(): ?int
    {
        return $this->maintenance_commission;
    }

    public function setMaintenanceCommission(?int $maintenance_commission): self
    {
        $this->maintenance_commission = $maintenance_commission;

        return $this;
    }

    public function getMaintenanceCommissionOwnerId(): ?int
    {
        return $this->maintenance_commission_owner_id;
    }

    public function setMaintenanceCommissionOwnerId(?int $maintenance_commission_owner_id): self
    {
        $this->maintenance_commission_owner_id = $maintenance_commission_owner_id;

        return $this;
    }

    /**
     * @return Collection|Products[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Products $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setStockpile($this);
        }

        return $this;
    }

    public function removeProduct(Products $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getStockpile() === $this) {
                $product->setStockpile(null);
            }
        }

        return $this;
    }

    // Resolve BUG not string
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|Structurs[]
     */
    public function getStructures(): Collection
    {
        return $this->structures;
    }

    public function addStructure(Structurs $structure): self
    {
        if (!$this->structures->contains($structure)) {
            $this->structures[] = $structure;
        }

        return $this;
    }

    public function removeStructure(Structurs $structure): self
    {
        if ($this->structures->contains($structure)) {
            $this->structures->removeElement($structure);
        }

        return $this;
    }

    /**
     * @return Collection|Structurs[]
     */
    public function getStructurs(): Collection
    {
        return $this->structurs;
    }

    public function addStructurs(Structurs $structurs): self
    {
        if (!$this->structurs->contains($structurs)) {
            $this->structurs[] = $structurs;
        }

        return $this;
    }

    public function removeStructurs(Structurs $structurs): self
    {
        if ($this->structurs->contains($structurs)) {
            $this->structurs->removeElement($structurs);
        }

        return $this;
    }

    /**
     * @return Collection|Structurs[]
     */


}
