<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */

    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */

    private $nick_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postal_code;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tel_number;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pro_tel_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $enabled;
//TODO afficher othermail dans form utilisateur//
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $other_mails = [];


    /**
     * @ORM\ManyToMany(targetEntity=Structurs::class, mappedBy="Admin_user_email")
     */
    private $structurs;

    public function __construct()
    {
        $this->admined_structurs = new ArrayCollection();
        $this->structurs = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }



    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getNickName(): ?string
    {
        return $this->nick_name;
    }

    public function setNickName(string $nick_name): self
    {
        $this->nick_name = $nick_name;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postal_code;
    }

    public function setPostalCode(int $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    public function getTelNumber(): ?int
    {
        return $this->tel_number;
    }

    public function setTelNumber(?int $tel_number): self
    {
        $this->tel_number = $tel_number;

        return $this;
    }

    public function getProTelNumber(): ?int
    {
        return $this->pro_tel_number;
    }

    public function setProTelNumber(?int $pro_tel_number): self
    {
        $this->pro_tel_number = $pro_tel_number;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getOtherMails(): ?array
    {
        return $this->other_mails;
    }

    public function setOtherMails(?array $other_mails): self
    {
        $this->other_mails = $other_mails;

        return $this;
    }

    // Resolve BUG not string
    public function __toString()
    {
        return $this->email;
    }

    /**
     * @return Collection|Structurs[]
     */
    public function getAdminedStructurs(): Collection
    {
        return $this->admined_structurs;
    }

    public function addAdminedStructur(Structurs $adminedStructur): self
    {
        if (!$this->admined_structurs->contains($adminedStructur)) {
            $this->admined_structurs[] = $adminedStructur;
        }

        return $this;
    }

    public function removeAdminedStructur(Structurs $adminedStructur): self
    {
        if ($this->admined_structurs->contains($adminedStructur)) {
            $this->admined_structurs->removeElement($adminedStructur);
        }

        return $this;
    }

    /**
     * @return Collection|Structurs[]
     */
    public function getStructurs(): Collection
    {
        return $this->structurs;
    }

    public function addStructur(Structurs $structur): self
    {
        if (!$this->structurs->contains($structur)) {
            $this->structurs[] = $structur;
            $structur->addAdminUserEmail($this);
        }

        return $this;
    }

    public function removeStructur(Structurs $structur): self
    {
        if ($this->structurs->removeElement($structur)) {
            $structur->removeAdminUserEmail($this);
        }

        return $this;
    }
}
