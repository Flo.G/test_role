<?php

namespace App\Entity;

use App\Repository\StructurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StructurRepository::class)
 */
class Structurs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\ManyToMany(targetEntity=User::class)
     */

    private $Admin_user_email;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Big;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $City;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Postal_code;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Tel_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description;

    /**
     * @ORM\ManyToMany(targetEntity=Stockpiles::class)
     */
    private $Stockpiles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Siret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Logo;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $Image = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Tva;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $Enabled;

    public function __construct()
    {
        $this->Admin_user_email = new ArrayCollection();
        $this->Stockpiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAdminUserEmail(): Collection
    {
        return $this->Admin_user_email;
    }

    public function addAdminUserEmail(User $adminUserEmail): self
    {
        if (!$this->Admin_user_email->contains($adminUserEmail)) {
            $this->Admin_user_email[] = $adminUserEmail;
        }

        return $this;
    }

    public function removeAdminUserEmail(User $adminUserEmail): self
    {
        $this->Admin_user_email->removeElement($adminUserEmail);

        return $this;
    }

    public function getBig(): ?int
    {
        return $this->Big;
    }

    public function setBig(?int $Big): self
    {
        $this->Big = $Big;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->Adress;
    }

    public function setAdress(?string $Adress): self
    {
        $this->Adress = $Adress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->City;
    }

    public function setCity(?string $City): self
    {
        $this->City = $City;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->Postal_code;
    }

    public function setPostalCode(?int $Postal_code): self
    {
        $this->Postal_code = $Postal_code;

        return $this;
    }

    public function getTelNumber(): ?int
    {
        return $this->Tel_number;
    }

    public function setTelNumber(?int $Tel_number): self
    {
        $this->Tel_number = $Tel_number;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|Stockpiles[]
     */
    public function getStockpiles(): Collection
    {
        return $this->Stockpiles;
    }

    public function addStockpile(Stockpiles $stockpile): self
    {
        if (!$this->Stockpiles->contains($stockpile)) {
            $this->Stockpiles[] = $stockpile;
        }

        return $this;
    }

    public function removeStockpile(Stockpiles $stockpile): self
    {
        $this->Stockpiles->removeElement($stockpile);

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->Siret;
    }

    public function setSiret(?string $Siret): self
    {
        $this->Siret = $Siret;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(?string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->Logo;
    }

    public function setLogo(?string $Logo): self
    {
        $this->Logo = $Logo;

        return $this;
    }

    public function getImage(): ?array
    {
        return $this->Image;
    }

    public function setImage(?array $Image): self
    {
        $this->Image = $Image;

        return $this;
    }

    public function getTva(): ?int
    {
        return $this->Tva;
    }

    public function setTva(?int $Tva): self
    {
        $this->Tva = $Tva;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->Enabled;
    }

    public function setEnabled(?bool $Enabled): self
    {
        $this->Enabled = $Enabled;

        return $this;
    }



}
