<?php

namespace App\Entity;

use App\Repository\ProductsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductsRepository::class)
 */
class Products
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disponibility;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Stockpiles", inversedBy="products")
     */
    private $stockpile;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     */
    private $category;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sub_category_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tag_type_usage_limitation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $geografic_limitation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $return_default_engagement_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * @ORM\Column(type="integer")
     */
    private $big_cost;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deposit_value_exeption;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tva_option;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $degressive_rate_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $delivery_category_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $size = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reservation_dates_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $control_documentation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getDisponibility(): ?bool
    {
        return $this->disponibility;
    }

    public function setDisponibility(bool $disponibility): self
    {
        $this->disponibility = $disponibility;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getStockpileId(): ?int
    {
        return $this->stockpile_id;
    }

    public function setStockpileId(int $stockpile_id): self
    {
        $this->stockpile_id = $stockpile_id;

        return $this;
    }

    public function getCategoryId(): ?int
    {
        return $this->category_id;
    }

    public function setCategoryId(int $category_id): self
    {
        $this->category_id = $category_id;

        return $this;
    }

    public function getSubCategoryId(): ?int
    {
        return $this->sub_category_id;
    }

    public function setSubCategoryId(int $sub_category_id): self
    {
        $this->sub_category_id = $sub_category_id;

        return $this;
    }

    public function getTagTypeUsageLimitation(): ?string
    {
        return $this->tag_type_usage_limitation;
    }

    public function setTagTypeUsageLimitation(?string $tag_type_usage_limitation): self
    {
        $this->tag_type_usage_limitation = $tag_type_usage_limitation;

        return $this;
    }

    public function getGeograficLimitation(): ?string
    {
        return $this->geografic_limitation;
    }

    public function setGeograficLimitation(?string $geografic_limitation): self
    {
        $this->geografic_limitation = $geografic_limitation;

        return $this;
    }

    public function getReturnDefaultEngagementId(): ?int
    {
        return $this->return_default_engagement_id;
    }

    public function setReturnDefaultEngagementId(?int $return_default_engagement_id): self
    {
        $this->return_default_engagement_id = $return_default_engagement_id;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(?int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getBigCost(): ?int
    {
        return $this->big_cost;
    }

    public function setBigCost(?int $big_cost): self
    {
        $this->big_cost = $big_cost;

        return $this;
    }

    public function getDepositValueExeption(): ?int
    {
        return $this->deposit_value_exeption;
    }

    public function setDepositValueExeption(?int $deposit_value_exeption): self
    {
        $this->deposit_value_exeption = $deposit_value_exeption;

        return $this;
    }

    public function getTvaOption(): ?int
    {
        return $this->tva_option;
    }

    public function setTvaOption(?int $tva_option): self
    {
        $this->tva_option = $tva_option;

        return $this;
    }

    public function getDegressiveRateId(): ?int
    {
        return $this->degressive_rate_id;
    }

    public function setDegressiveRateId(?int $degressive_rate_id): self
    {
        $this->degressive_rate_id = $degressive_rate_id;

        return $this;
    }

    public function getDeliveryCategoryId(): ?int
    {
        return $this->delivery_category_id;
    }

    public function setDeliveryCategoryId(?int $delivery_category_id): self
    {
        $this->delivery_category_id = $delivery_category_id;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(?int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getSize(): ?array
    {
        return $this->size;
    }

    public function setSize(?array $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getReservationDatesId(): ?int
    {
        return $this->reservation_dates_id;
    }

    public function setReservationDatesId(?int $reservation_dates_id): self
    {
        $this->reservation_dates_id = $reservation_dates_id;

        return $this;
    }

    public function getControlDocumentation(): ?string
    {
        return $this->control_documentation;
    }

    public function setControlDocumentation(string $control_documentation): self
    {
        $this->control_documentation = $control_documentation;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStockpile(): ?Stockpiles
    {
        return $this->stockpile;
    }

    public function setStockpile(?Stockpiles $stockpile): self
    {
        $this->stockpile = $stockpile;

        return $this;
    }
}

