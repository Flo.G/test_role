<?php

namespace App\Form;

use App\Entity\Structurs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StructursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('Name')
            ->add('Big')
            ->add('Adress')
            ->add('City')
            ->add('Postal_code')
            ->add('Tel_number')
            ->add('Description')
            ->add('Siret')
            ->add('Type')
            ->add('Logo')
            //->add('Image')
            ->add('Tva')
            ->add('Enabled')
            ->add('Admin_user_email')
            ->add('Stockpiles')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Structurs::class,
        ]);
    }
}
