<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103145857 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE stockpiles_structurs');
        $this->addSql('DROP INDEX UNIQ_9BD582BB5E237E06 ON structurs');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stockpiles_structurs (stockpiles_id INT NOT NULL, structurs_id INT NOT NULL, INDEX IDX_FE254A79504608C8 (structurs_id), INDEX IDX_FE254A7974DC925D (stockpiles_id), PRIMARY KEY(stockpiles_id, structurs_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE stockpiles_structurs ADD CONSTRAINT FK_FE254A79504608C8 FOREIGN KEY (structurs_id) REFERENCES structurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stockpiles_structurs ADD CONSTRAINT FK_FE254A7974DC925D FOREIGN KEY (stockpiles_id) REFERENCES stockpiles (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9BD582BB5E237E06 ON structurs (name)');
    }
}
