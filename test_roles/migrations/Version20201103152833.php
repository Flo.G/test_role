<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103152833 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_structurs');
        $this->addSql('ALTER TABLE structurs ADD enabled TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_structurs (user_id INT NOT NULL, structurs_id INT NOT NULL, INDEX IDX_9B350F5FA76ED395 (user_id), INDEX IDX_9B350F5F504608C8 (structurs_id), PRIMARY KEY(user_id, structurs_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_structurs ADD CONSTRAINT FK_9B350F5F504608C8 FOREIGN KEY (structurs_id) REFERENCES structurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_structurs ADD CONSTRAINT FK_9B350F5FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structurs DROP enabled');
    }
}
