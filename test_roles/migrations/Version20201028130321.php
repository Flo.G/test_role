<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201028130321 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, structure_id INT NOT NULL, affiliated_user_id INT DEFAULT NULL, date_start DATE NOT NULL, date_end DATE NOT NULL, active TINYINT(1) NOT NULL, budget INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products (id INT AUTO_INCREMENT NOT NULL, stockpile_id INT DEFAULT NULL, category_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, disponibility TINYINT(1) NOT NULL, reference VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, sub_category_id INT DEFAULT NULL, tag_type_usage_limitation VARCHAR(255) DEFAULT NULL, geografic_limitation VARCHAR(255) DEFAULT NULL, return_default_engagement_id INT DEFAULT NULL, value INT NOT NULL, big_cost INT NOT NULL, deposit_value_exeption INT DEFAULT NULL, tva_option INT DEFAULT NULL, degressive_rate_id INT DEFAULT NULL, delivery_category_id INT DEFAULT NULL, weight INT DEFAULT NULL, size LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', reservation_dates_id INT DEFAULT NULL, control_documentation VARCHAR(255) DEFAULT NULL, INDEX IDX_B3BA5A5AA4D0E58D (stockpile_id), INDEX IDX_B3BA5A5A12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rents (id INT AUTO_INCREMENT NOT NULL, products_id LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', date_start DATE NOT NULL, date_end DATE NOT NULL, stage_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stages (id INT AUTO_INCREMENT NOT NULL, adress VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, postal_code INT NOT NULL, event_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stockpiles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, adress VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, postal_code INT NOT NULL, contact_user_id INT DEFAULT NULL, contract_content LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', deposit_value INT DEFAULT NULL, stock_commission INT DEFAULT NULL, stock_commission_owner_id INT DEFAULT NULL, maintenance_commission INT DEFAULT NULL, maintenance_commission_owner_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stockpiles_structurs (stockpiles_id INT NOT NULL, structurs_id INT NOT NULL, INDEX IDX_FE254A7974DC925D (stockpiles_id), INDEX IDX_FE254A79504608C8 (structurs_id), PRIMARY KEY(stockpiles_id, structurs_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structurs (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, big INT DEFAULT NULL, adress VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, postal_code INT DEFAULT NULL, tel_number INT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, siret VARCHAR(255) DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, image LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', tva INT DEFAULT NULL, UNIQUE INDEX UNIQ_9BD582BB5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structurs_user (structurs_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_E90645ED504608C8 (structurs_id), INDEX IDX_E90645EDA76ED395 (user_id), PRIMARY KEY(structurs_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structurs_stockpiles (structurs_id INT NOT NULL, stockpiles_id INT NOT NULL, INDEX IDX_7CC176F6504608C8 (structurs_id), INDEX IDX_7CC176F674DC925D (stockpiles_id), PRIMARY KEY(structurs_id, stockpiles_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, nick_name VARCHAR(255) DEFAULT NULL, adress VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, postal_code INT DEFAULT NULL, tel_number INT DEFAULT NULL, pro_tel_number INT DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, enabled TINYINT(1) DEFAULT \'1\', other_mails LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_structurs (user_id INT NOT NULL, structurs_id INT NOT NULL, INDEX IDX_9B350F5FA76ED395 (user_id), INDEX IDX_9B350F5F504608C8 (structurs_id), PRIMARY KEY(user_id, structurs_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5AA4D0E58D FOREIGN KEY (stockpile_id) REFERENCES stockpiles (id)');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5A12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE stockpiles_structurs ADD CONSTRAINT FK_FE254A7974DC925D FOREIGN KEY (stockpiles_id) REFERENCES stockpiles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stockpiles_structurs ADD CONSTRAINT FK_FE254A79504608C8 FOREIGN KEY (structurs_id) REFERENCES structurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structurs_user ADD CONSTRAINT FK_E90645ED504608C8 FOREIGN KEY (structurs_id) REFERENCES structurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structurs_user ADD CONSTRAINT FK_E90645EDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structurs_stockpiles ADD CONSTRAINT FK_7CC176F6504608C8 FOREIGN KEY (structurs_id) REFERENCES structurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structurs_stockpiles ADD CONSTRAINT FK_7CC176F674DC925D FOREIGN KEY (stockpiles_id) REFERENCES stockpiles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_structurs ADD CONSTRAINT FK_9B350F5FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_structurs ADD CONSTRAINT FK_9B350F5F504608C8 FOREIGN KEY (structurs_id) REFERENCES structurs (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE products DROP FOREIGN KEY FK_B3BA5A5A12469DE2');
        $this->addSql('ALTER TABLE products DROP FOREIGN KEY FK_B3BA5A5AA4D0E58D');
        $this->addSql('ALTER TABLE stockpiles_structurs DROP FOREIGN KEY FK_FE254A7974DC925D');
        $this->addSql('ALTER TABLE structurs_stockpiles DROP FOREIGN KEY FK_7CC176F674DC925D');
        $this->addSql('ALTER TABLE stockpiles_structurs DROP FOREIGN KEY FK_FE254A79504608C8');
        $this->addSql('ALTER TABLE structurs_user DROP FOREIGN KEY FK_E90645ED504608C8');
        $this->addSql('ALTER TABLE structurs_stockpiles DROP FOREIGN KEY FK_7CC176F6504608C8');
        $this->addSql('ALTER TABLE user_structurs DROP FOREIGN KEY FK_9B350F5F504608C8');
        $this->addSql('ALTER TABLE structurs_user DROP FOREIGN KEY FK_E90645EDA76ED395');
        $this->addSql('ALTER TABLE user_structurs DROP FOREIGN KEY FK_9B350F5FA76ED395');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE events');
        $this->addSql('DROP TABLE products');
        $this->addSql('DROP TABLE rents');
        $this->addSql('DROP TABLE stages');
        $this->addSql('DROP TABLE stockpiles');
        $this->addSql('DROP TABLE stockpiles_structurs');
        $this->addSql('DROP TABLE structurs');
        $this->addSql('DROP TABLE structurs_user');
        $this->addSql('DROP TABLE structurs_stockpiles');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_structurs');
    }
}
